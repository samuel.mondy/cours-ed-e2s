if (!requireNamespace("BiocManager", quietly = TRUE))
  install.packages("BiocManager")

BiocManager::install("DESeq2")
BiocManager::install("preprocessCore")
BiocManager::install("vegan")
BiocManager::install("limma")
install.packages("EnvStats")
Ten_max<-function(ref,index=20)
  
{
  vref<-c()
  for(i in 1:dim(ref)[2])
  {
    seuil<-sort(ref[,i],decreasing=T)[index]
    temp<-which(ref[,i]>(seuil-1))
    vref<-unique(c(vref,temp))
  }
  
  return(ref[vref,])
  
}


library(vegan)
library(gplots)
library(DESeq2)
library(EnvStats)
library(preprocessCore)
library(limma)

#Utilisation de jeux de données simulés

#bibliographie
#https://genomebiology.biomedcentral.com/articles/10.1186/gb-2010-11-10-r106
#assomption les distributions des matrices de comptage suivent plutôt une négative binomiale plus qu'une loi de poisson

#normalité des test de micrarray
#http://www.cs.cmu.edu/~epxing/Class/10810/lecture/recitation7.pdf

#Définition du nombre d'OTU de la matrice
OTU=400
#Probabilité de succès
prob=0.00001
size=0.2
#nombre de réplicats biologiques
replicate=10
#nombre de reads minimum
pop=10000


#génération d'une distribution négative binomiale
a <- as.matrix(ceiling(rnbinom(OTU,size=size,prob=prob)))
#nom des OTU
rownames(a)<-paste("OTU_",seq(1:OTU),sep="")
#génération d'une deuxième distribution négative binomiale
a<-cbind(a,as.matrix(ceiling(rnbinom(OTU,size=size,prob=0.0001))))
barplot(100*apply(a[as.vector(which(apply(a,1,function(x) length(which(x==0)))==0)),],2,sum)/apply(a,2,sum),main="pourcentage de séquences sans ligne avec un zéro")
#création d'un échantillon C
#Identique à B mais pourlequel une OTU est devenu majoritaire et représente la moitié de la communauté
a<-cbind(a,round(a[,2]/2,digit=0))
a[sample(1:OTU,1),3]<-sum(a[,2])/2
a<-cbind(a,sample(a[,2]))
colnames(a)<-c("A","B","C","D")


par(mfrow=c(1,2))
plot(density(a[,1]))
plot(density(log(a[,1])))




barplot(100*apply(a[as.vector(which(apply(a,1,function(x) length(which(x==0)))==0)),],2,sum)/apply(a,2,sum),main="pourcentage de séquences sans ligne avec un zéro")

pop=50000
plot(c(rarefy(a[,1],sample=pop),rarefy(a[,2],sample=pop),rarefy(a[,3],sample=pop),rarefy(a[,4],sample=pop)),ylim=c(0,400),main="Richness")
pop=10000
points(c(rarefy(a[,1],sample=pop),rarefy(a[,2],sample=pop),rarefy(a[,3],sample=pop),rarefy(a[,4],sample=pop)),ylim=c(0,400),col="red")
pop=500000
points(c(rarefy(a[,1],sample=pop),rarefy(a[,2],sample=pop),rarefy(a[,3],sample=pop),rarefy(a[,4],sample=pop)),ylim=c(0,400),col="green",pch=2)
pop=max(apply(a,2,sum))
points(c(rarefy(a[,1],sample=pop),rarefy(a[,2],sample=pop),rarefy(a[,3],sample=pop),rarefy(a[,4],sample=pop)),ylim=c(0,400),col="orange",pch=3)

d<-density(a)
plot(d,type="n")
color=c("black","green","blue")
for(i in 1:3)
{
  temp<-density(a[,i])
  #temp$y<-temp$y/max(d$y)
  lines(temp,col=color[i])
}
apply(a,2,sum)
loga<-log2(a)
loga[loga=="-Inf"]<-c(0)
heatmap.2(loga,trace="none")
heatmap.2(scale(a))

z<-matrix(0,ncol=4*replicate,nrow=OTU)
rownames(z)<-sort(rownames(a))
b<-apply(a,2,function(x) x/sum(x))
count<-c(0)
for(i in 1:4)
{
  for(j in 1:replicate)
  {
    print(j+i+count-1)
    h<-sample(1:OTU,prob=b[,i],size=pop,replace=TRUE)
    l<-paste("OTU_",h,sep="")
    k<-table(l)
    #print(k)
    z[which(rownames(z)%in%names(k)),j+i+count-1]<-k
  }
  count<-count+replicate-1
}
colnames(z)<-c(rep("A",replicate),rep("B",replicate),rep("C",replicate),rep("D",replicate))

temp <- log2(z)
temp[temp=="-Inf"] <- c(0)
heatmap.2(temp,col=greenred,trace="none")
heatmap.2(scale(z),col=greenred,trace="none")
#déterminer le nombre de lecteurs qui sont dans des lignes sans 0
threshold <- mean(apply(z[as.vector(which(apply(z,1,function(x) length(which(x==0)))==0)),],2,sum)/apply(z,2,sum))
print(threshold)


coldata<-as.matrix(c(rep("A",replicate),rep("B",replicate),rep("C",replicate),rep("D",replicate)))
colnames(coldata)<-"condition"
dds<-DESeqDataSetFromMatrix(countData = z,
                            colData = coldata,
                            design= ~ condition)
diagdds<-DESeq(dds,test="Wald",fitType = "parametric")

#minReplicatesForReplace = 7
#minReplicatesForReplace	
#the minimum number of replicates required in order to use replaceOutliers on a sample.
#If there are samples with so many replicates, the model will be refit after these replacing outliers
#flagged by Cook's distance. Set to Inf in order to never replace outliers.

temp2<-estimateSizeFactors(diagdds,type="ratio")
print(sizeFactors(temp2))
temp2<-estimateSizeFactors(diagdds,type="poscounts")
print(sizeFactors(temp2))
temp2<-estimateSizeFactors(diagdds,type="iterate")
print(sizeFactors(temp2))

heatmap(z)
temp<-levels(diagdds$condition)
e<-combn(length(temp),2)
x<-counts(diagdds,normalized=TRUE)
heatmap(x)
res<-results(diagdds)
heatmap(counts(diagdds,normalized=TRUE))
for (k in 1:dim(e)[2])
{
  res<-results(diagdds,contrast=c("condition",temp[e[1,k]],temp[e[2,k]]))
  print(res)
  write.table(res,file=paste("BARIC",temp[e[1,k]],temp[e[2,k]],"res_raw.xls",sep="_"),quote=F,sep="\t")
}

geomean<-apply(z,1,function(x) geoMean(x))
length(which(is.na(geomean)))
sum(z[which(is.na(geomean)),])/sum(z)

y1<-normalize.quantiles(z)
y<-normalizeQuantiles(z,ties = FALSE)
w<-decostand(z,method="total")
v<-decostand(z,method="max")
t<-decostand(z,method="hellinger")

par(mfrow=c(1,2))
heatmap(x,main="geomean")
heatmap(y,main="normalize quantiles")
heatmap(z,main="normalized count")
heatmap(w,main="total")
heatmap(v,main="max")
heatmap(t,main="hellinger")


par(mfrow=c(1,5))
for (i in 1:5)
{
  boxplot(Ten_max(x)[i,]~as.factor(coldata),main="Normalized geometric mean",ylim=c(0,max(x[,i])))
}
par(mfrow=c(1,5))
for (i in 1:5)
{
  boxplot(Ten_max(y)[i,]~as.factor(coldata),main="Normalized quantiles",ylim=c(0,max(x[,i])))
}
par(mfrow=c(1,5))
for (i in 1:5)
{
  boxplot(Ten_max(z)[i,]~as.factor(coldata),main="Normalized count",ylim=c(0,max(x[,i])))
}
par(mfrow=c(1,5))
for (i in 1:5)
{
  boxplot(Ten_max(t)[i,]~as.factor(coldata),main="hellinger",ylim=c(0,max(t[,i])))
}

par(mfrow=c(2,4))
boxplot(x[1,]~as.factor(coldata),main="Normalized geometric mean")
boxplot(y[1,]~as.factor(coldata),main="Normalized quantiles")
boxplot(z[1,]~as.factor(coldata),main="Normalized count")
boxplot(w[1,]~as.factor(coldata),main="decostand total")
boxplot(v[1,]~as.factor(coldata),main="decostand max")
boxplot(t[1,]~as.factor(coldata),main="hellinger")


par(mfrow=c(2,4))
plot(x[,11],x[,21],main="Normalized geometric mean")
plot(y[,11],y[,21],main="Normalized quantiles")
plot(z[,11],z[,21],main="Normalized count")
plot(w[,11],w[,21],main="decostand total")
plot(v[,11],v[,21],main="decostand max")
plot(t[,11],t[,21],main="hellinger")
par(mfrow=c(2,4))
plot(sort(x[,11]),sort(x[,21]),main="Normalized geometric mean")
plot(sort(y[,11]),sort(y[,21]),main="Normalized quantiles")
plot(sort(z[,11]),sort(z[,21]),main="Normalized count")
plot(sort(w[,11]),sort(w[,21]),main="decostand total")
plot(sort(v[,11]),sort(v[,21]),main="decostand max")
plot(sort(t[,11]),sort(t[,21]),main="hellinger")

par(mfrow=c(2,4))
plot(x[,1],x[,40],main="Normalized geometric mean")
plot(y[,1],y[,40],main="Normalized quantiles")
plot(z[,1],z[,40],main="Normalized count")
plot(w[,1],w[,40],main="decostand total")
plot(v[,1],v[,40],main="decostand max")
plot(t[,1],t[,40],main="hellinger")
par(mfrow=c(2,4))
plot(sort(x[,1]),sort(x[,40]))
plot(sort(y[,1]),sort(y[,40]))
plot(sort(z[,1]),sort(z[,40]))
plot(sort(w[,1]),sort(w[,40]))
plot(sort(v[,1]),sort(v[,40]))
plot(sort(t[,1]),sort(t[,40]))

par(mfrow=c(2,4))
plot(x[,21],x[,40],main="Normalized geometric mean")
plot(y[,21],y[,40],main="Normalized quantiles")
plot(z[,21],z[,40],main="Normalized count")
plot(w[,21],w[,40],main="decostand total")
plot(v[,21],v[,40],main="decostand max")
plot(t[,21],t[,40],main="hellinger")
par(mfrow=c(2,4))
plot(sort(x[,21]),sort(x[,40]))
plot(sort(y[,21]),sort(y[,40]))
plot(sort(z[,21]),sort(z[,40]))
plot(sort(w[,21]),sort(w[,40]))
plot(sort(v[,21]),sort(v[,40]))
plot(sort(t[,21]),sort(t[,40]))


par(mfrow=c(2,4))
plot(z[,11],x[,11],main="Normalized geometric mean")
plot(z[,11],y[,11],main="Normalized quantiles")
plot(z[,11],z[,11],main="Normalized count")
plot(z[,11],w[,11],main="decostand total")
plot(z[,11],v[,11],main="decostand max")
plot(z[,11],t[,11],main="hellinger")
par(mfrow=c(2,4))
plot(sort(z[,11]),sort(x[,11]))
plot(sort(z[,11]),sort(y[,11]))
plot(sort(z[,11]),sort(z[,11]))
plot(sort(z[,11]),sort(w[,11]))
plot(sort(z[,11]),sort(v[,11]))
plot(sort(z[,11]),sort(t[,11]))



#EdgeR / TMM
